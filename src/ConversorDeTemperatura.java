
/**
 * esse programa � um conversor
 * que converte temperaturas em �K, �F ou �C
 * @author Iris Campanella Cabral
 */

import javax.swing.JOptionPane;

public class ConversorDeTemperatura {

	public static void main(String[] args) {

		// declara a variavel resposta
		int resposta;

		/*repete o programa 
		 * enquanto a resposta for 1("s")
		 */

		do {
			String tipo = JOptionPane.showInputDialog(null,
					"Qual unidade de medida? 1(Fahrenheit) / 2(Kelvin) / 3(Celsius)",
					"BEM VINDO AO CONVERSOR DE TEMPERATURAS!!!", JOptionPane.QUESTION_MESSAGE);
			int tipoTemperatura = Integer.parseInt(tipo);
			switch (tipoTemperatura) {
			case 1:
				fahrenheit();
				break;
			case 2:
				kelvin();
				break;
			case 3:
				celsius();
				break;
			default:
				JOptionPane.showMessageDialog(null, "Escolha 1, 2 ou 3!", "Escolha errada!!!",
						JOptionPane.ERROR_MESSAGE);
			}
			String resp = JOptionPane.showInputDialog(null, "Deseja calcular novamente? 1(S) / 2 (N)",
					" CONFIRMA��O...", JOptionPane.QUESTION_MESSAGE);
			resposta = Integer.parseInt(resp);
		} while (resposta == 1);

	}

	// fun��es de convers�es
	public static void fahrenheit() {

		String temperatura = JOptionPane.showInputDialog(null, "Informe a temperatura: ",
				"Informando a temperatura para convers�o...", JOptionPane.WARNING_MESSAGE).replace(",", ".");
		double temperaturas = Double.parseDouble(temperatura);
		double kelvin;
		double celsius;
		celsius = (temperaturas - 32) / 1.8;
		kelvin = (temperaturas - 32) / 1.8 + 273.15;
		JOptionPane.showMessageDialog(null, "Convers�o em Kelvin: " + kelvin + "�", "Sua convers�o...",
				JOptionPane.INFORMATION_MESSAGE);
		JOptionPane.showMessageDialog(null, "Convers�o em Celsius: " + celsius + "�", "Sua convers�o...",
				JOptionPane.INFORMATION_MESSAGE);
	}

	public static void celsius() {

		String temperatura = JOptionPane.showInputDialog(null, "Informe a temperatura: ",
				"Informando a temperatura para convers�o...", JOptionPane.WARNING_MESSAGE).replace(",", ".");
		double temperaturas = Double.parseDouble(temperatura);
		double kelvin;
		double fahren;
		kelvin = temperaturas + 273.15;
		fahren = (temperaturas * 1.8) / 32;
		JOptionPane.showMessageDialog(null, "Convers�o em Kelvin: " + kelvin, "Sua convers�o...",
				JOptionPane.INFORMATION_MESSAGE);
		JOptionPane.showMessageDialog(null, "Convers�o em Fahrenheit: " + fahren, "Sua convers�o...",
				JOptionPane.INFORMATION_MESSAGE);

	}

	public static void kelvin() {

		String temperatura = JOptionPane.showInputDialog(null, "Informe a temperatura: ",
				"Informando a temperatura para convers�o...", JOptionPane.WARNING_MESSAGE).replace(",", ".");
		double temperaturas = Double.parseDouble(temperatura);
		double celsius;
		double fahren;
		celsius = temperaturas - 273.15;
		fahren = 1.8 * (temperaturas - 271.15) + 32;
		JOptionPane.showMessageDialog(null, "Convers�o em Fahrenheit: " + fahren, "Sua convers�o...",
				JOptionPane.INFORMATION_MESSAGE);
		JOptionPane.showMessageDialog(null, "Convers�o em Celsius: " + celsius, "Sua convers�o...",
				JOptionPane.INFORMATION_MESSAGE);
	}

}
